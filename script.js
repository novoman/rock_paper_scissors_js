const CHOICES = ['rock', 'paper', 'scissors'];

const CHOICES_BUTTONS = document.querySelectorAll('#choices button');

// if computer wins round then winnerCounter--, else if user winnerCounter++, if draw - nothing
let winnerCounter = 0;

function computerSelection() {
	return CHOICES[Math.floor(Math.random() * CHOICES.length)];
}

function playerSelection(button) {
	let choice = button.textContent;
	return choice.toLowerCase();
}

function playRound(playerSelection, computerSelection) {
	if (playerSelection === 'rock') {
		if (computerSelection === 'rock')
			return 'Draw! You both chose Rock';
		if (computerSelection === 'paper') {
			winnerCounter--;
			return 'You Lose! Paper beats Rock';
		}
		if (computerSelection === 'scissors') {
			winnerCounter++;
			return 'You Win! Rock beats Scissors';
		}
	}
	else if (playerSelection === 'paper') {
		if (computerSelection === 'rock') {
			winnerCounter++;
			return 'You Win! Paper beats Rock';
		}
		if (computerSelection === 'paper')
			return 'Draw! You both chose Paper';
		if (computerSelection === 'scissors') {
			winnerCounter--;
			return 'You Lose! Scissors beats Paper';
		}
	}
	else if (playerSelection === 'scissors') {
		if (computerSelection === 'rock') {
			winnerCounter--;
			return 'You Lose! Rock beats Scissors';
		}
		if (computerSelection === 'paper') {
			winnerCounter++;
			return 'You Win! Scissors beats Paper';
		}
		if (computerSelection === 'scissors')
			return 'Draw! You both chose Scissors';
	}
	else {
		return 'You entered wrong choice!';
	}
}

function checkGameResult() {
	switch (true) {
		case winnerCounter >= 5:
			alert('Congratulations! You win!');
			break;
		case winnerCounter <= -5:
			alert('Never lucky! You loose!');
			console.log('Even here you loose :P');
			break;	
	}
}

function updateResultView() {
	document.getElementById('result').textContent = 'Your score: ' + winnerCounter;
}

CHOICES_BUTTONS.forEach((button) => {
  button.addEventListener('click', (e) => {
    alert(playRound(playerSelection(button), computerSelection()));
    updateResultView();
    checkGameResult();
  });
});
